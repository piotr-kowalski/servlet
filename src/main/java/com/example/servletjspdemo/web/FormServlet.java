package com.example.servletjspdemo.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/form")
public class FormServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		out.println("<html><body><h2>Simple form servlet</h2>" +
				"<form action='data'>" +
				"Imie: <input type='text' name='firstName'><br />" +
				"Nazwisko: <input type='text' name='lastName'><br />" +
				"Email: <input type='text' name='email'><br />"+
				"Potwierdz Email: <input type='text' name='emailConf><br />"+
				"Nazwa Pracodawcy: <input type='text' name='job'><br />"+
				"Skad dowiedziales sie o konferencji: <input type='text' name='rec'><br />"+
				"<input type='radio' name='info' value='adJob'>Ogloszenie w pracy<br />"+
				"<input type='radio' name='info' value='adStudy'>Ogloszenie na uczelni<br />"+
				"<input type='radio' name='info' value='facebook'>Facebook<br />"+
				"<input type='radio' name='info' value='friend'>Znajomi<br />"+
				"<input type='radio' name='info' value='other'>Inne"+
				"<input type='text' name='other'><br />"+
				"<input type='submit' value=' OK ' />" +
				"</form>" +
				"</body></html>");
		out.close();
	}

}
